package com.library.elib.Integration

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.library.elib.ElibApplication
import com.library.elib.controllers.BookPdfController
import com.library.elib.dto.BookDto
import com.library.elib.entities.Author
import com.library.elib.services.BookPdfService
import org.junit.Before
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.*
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import java.io.FileInputStream
import java.util.*
import kotlin.collections.HashMap


@RunWith(SpringRunner::class)
@ActiveProfiles("integrationTest")
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = [ElibApplication::class, BookPdfService::class])
class BookPdfControllerTest  {

    @Autowired
    private val ctx: WebApplicationContext? = null

    @Autowired
    private var mockMvc: MockMvc = MockMvcBuilders.standaloneSetup(BookPdfController()).build().also { mockMvc = it }

    @Autowired
    private var objectMapper: ObjectMapper = ObjectMapper()

    @Before
    fun init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.ctx as WebApplicationContext).build()
    }

    private val mockAuthor = Author(id = 1, "Daari Wala")
    private val mockBookDto = BookDto(
        id = 1,
        name = "Choor ki daari mein tinka",
        page_no = 420,
        isbn_no = "ISBN 1-56389-668-1",
        categories = listOf("DRAMA", "FICTION"),
        author_id = "1",
        added_on = Date()
    )

    @Test
    fun uploadPdfTest(){

        objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false)
        val mapper = objectMapper.writer().withDefaultPrettyPrinter()

        val file = FileInputStream("/home/kabirsethi/Downloads/elib/src/main/resources/static/books/Get_Started_With_Smallpdf.pdf")
        val mockMultiPartFile = MockMultipartFile("file", file)
        val contentTypeParams = HashMap<String, String>()
        contentTypeParams["boundary"] = "265001916915724"
        val mediaType = MediaType("multipart", "form-data", contentTypeParams)


        mockMvc.perform(post("/author/create")
            .content(mapper.writeValueAsString(mockAuthor))
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isCreated)

        mockMvc.perform(post("/book/create")
            .content(mapper.writeValueAsString(mockBookDto))
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isCreated)

        val res = mockMvc.perform(
            multipart("/book_pdf/upload")
                .file("file", mockMultiPartFile.bytes)
                .param("book_id", 1.toString())
                .contentType(mediaType))

            .andExpect(status().isCreated);
    }

}