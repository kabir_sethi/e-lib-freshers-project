package com.library.elib.Integration

import com.library.elib.TestConfig.AuthorConfigurationTest
import com.fasterxml.jackson.databind.ObjectMapper
import com.library.elib.ElibApplication
import com.library.elib.controllers.AuthorController
import com.library.elib.entities.Author
import com.library.elib.repositories.AuthorRepository
import org.hamcrest.Matchers.notNullValue
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@ActiveProfiles("dev")
//@RunWith(SpringRunner::class)
//@SpringBootTest(classes = [ElibApplication::class] ,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@WebMvcTest(AuthorController::class)
@Import(AuthorConfigurationTest::class)
class AuthorControllerTest (){

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @MockBean
    private  lateinit var authorRepository: AuthorRepository

    private val author = Author(name = "Hello")


    @Test
    @Throws(Exception::class)
    fun getAuthors(){
        val jsonString = objectMapper.writeValueAsString(author)
        Mockito.`when`(authorRepository.save(author)).thenReturn(author)

        mockMvc.perform(post("/author/create")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonString)
        ).andExpect(status().isCreated)
            .andExpect(jsonPath("$", notNullValue()))

    }


}