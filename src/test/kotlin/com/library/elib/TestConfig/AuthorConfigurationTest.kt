package com.library.elib.TestConfig

import com.library.elib.repositories.AuthorRepository
import com.library.elib.services.AuthorService
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.core.JdbcTemplate
import javax.sql.DataSource

//@Profile("test")
@TestConfiguration
class AuthorConfigurationTest {


    @Bean
    fun dataSource(): DataSource {
        val dataSourceBuilder = DataSourceBuilder.create()
        dataSourceBuilder.driverClassName("com.mysql.cj.jdbc.Driver")
        dataSourceBuilder.url("jdbc:mysql://localhost:3306/elib_db_test?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true&useSSL=false")
        dataSourceBuilder.username("root")
        dataSourceBuilder.password("password")
        return dataSourceBuilder.build()
    }

    @Bean
    fun authorService(authorRepository: AuthorRepository): AuthorService {
        return  AuthorService()
    }

    @Bean
    fun jdbcTemplate(): JdbcTemplate{
        return  JdbcTemplate(dataSource())
    }

}