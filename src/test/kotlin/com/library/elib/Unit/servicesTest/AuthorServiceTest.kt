package com.library.elib.Unit.servicesTest

import com.library.elib.services.AuthorService
import com.library.elib.dto.AuthorDto
import com.library.elib.entities.Author
import com.library.elib.repositories.AuthorRepository
import com.library.elib.utils.EntityToDtoMapper
import org.junit.Assert
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock

import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.web.servlet.MockMvc
import java.util.*

class AuthorServiceTest {

    @Mock
    private lateinit var authorRepository: AuthorRepository

    @Mock
    private  lateinit var  jdbcTemplate: JdbcTemplate

    @InjectMocks
    private lateinit var authorService: AuthorService

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    val author = Author(id = 1, name = "Bread")

    @Test
    fun getAuthorByIdIsWorking() {
        val mockAuthor = Author(id = 1, name = "auth")
        val mockId: Long = 1
        if(this::authorRepository.isInitialized){
            Mockito.`when`(authorRepository.findById(mockId)).thenReturn(Optional.of(mockAuthor))
            val result = authorService.getAuthorById(mockId)
            Assert.assertEquals(mockAuthor, result)
        }
    }

   @Test
   fun pageSearchTest() {
       val statement = "SELECT * from author LIMIT ? OFFSET ?"
       val limit = 1
       val offset = 0
       val intermediate = listOf(mapOf("id" to (1).toLong(), "name" to "Spring"))
       Mockito.`when`(jdbcTemplate.queryForList(statement, limit, offset)).thenReturn(intermediate)
       if(this::authorService.isInitialized){
           val actual = authorService.pageSearch(limit, offset)
           val expected = listOf(AuthorDto(id = (1).toLong(), name = "Spring"))
           Assert.assertEquals(expected[0].name, actual[0].name)
       }
   }
}