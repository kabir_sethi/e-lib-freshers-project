package com.library.elib.Unit.servicesTest

import com.library.elib.dto.BookDto
import com.library.elib.entities.Author
import com.library.elib.entities.Book
import com.library.elib.repositories.AuthorRepository
import com.library.elib.repositories.BookRepository
import com.library.elib.services.BookService
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.BeforeEach
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.springframework.core.convert.ConversionService
import java.util.*

class BookServiceUnitTest {

    @InjectMocks
    private lateinit var bookService: BookService

    @Mock
    private lateinit var bookRepository: BookRepository

    @Mock
    private lateinit var authorRepository: AuthorRepository

    @Mock
    private lateinit var conversionService: ConversionService

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.initMocks(this)

    }

    private val mockAuthor = Author(id = 1, "Paani Wala")
    private val mockBook = Book(
        id = 5,
        name = "Chao",
        page_no = 420,
        isbn_no = "ISBN 1-56389-668-1",
        categories = listOf("DRAMA", "FICTION"),
        author = mockAuthor,
        added_on = Date()
    )
    private val mockBookDto = BookDto(
        id = 5,
        name = "Choa",
        page_no = 420,
        isbn_no = "ISBN 1-56389-668-1",
        categories = listOf("DRAMA", "FICTION"),
        author_id = "1",
        added_on = Date()
    )

    @Test
    fun addBookUnitTest(){
        Mockito.`when`(bookRepository.save(mockBook)).thenReturn(mockBook)
        Mockito.`when`(conversionService.convert(mockAuthor.id.toString(), Author::class.javaObjectType))
            .thenReturn(mockAuthor)
        print(bookRepository.save(mockBook))
        val actual = bookService.addBook(mockBookDto)
        Assert.assertNotNull(actual) // repo.save is returning null although moockito returns entity
        Assert.assertTrue(actual.name == mockBook.name)
    }

   @Test
    fun pageSearchTest(){
        val author = Author(id = 1, "James")
        Mockito.`when`(authorRepository.findById(1)).thenReturn(Optional.of(author))
        val res = bookService.pageSearch(1,0,1)
        Assert.assertTrue(res.isEmpty())
    }
}