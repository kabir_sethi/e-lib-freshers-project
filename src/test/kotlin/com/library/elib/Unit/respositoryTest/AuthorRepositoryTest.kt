package com.library.elib.Unit.respositoryTest

import com.library.elib.repositories.AuthorRepository
import com.library.elib.entities.Author
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.annotation.Rollback

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class AuthorRepositoryTest {

    @Autowired
    private lateinit var authorRepository: AuthorRepository

    @Test
    @Rollback(false)
    fun isPersistingData() {
        val author = Author(name = "Blob Clob")
        val result = authorRepository.save(author)
        Assert.assertEquals(result, author)
    }

}