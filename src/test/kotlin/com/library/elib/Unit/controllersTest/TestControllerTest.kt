package com.library.elib.Unit.controllersTest

import com.library.elib.controllers.TestController
import com.library.elib.services.TestService
import io.swagger.util.ObjectMapperFactory
import org.junit.Assert
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.web.servlet.MockMvc

internal class TestControllerTest {

    @Mock lateinit var testService: TestService

    @InjectMocks lateinit var testController: TestController

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun pleaseRun(){
        Mockito.`when`(testService.greet()).thenReturn("Hello")
        val res = testController.getGreetings()
        Assert.assertEquals("Hello", res)
    }

}