package com.library.elib.Unit.controllersTest

import com.fasterxml.jackson.databind.ObjectMapper
import com.library.elib.controllers.AuthorController
import com.library.elib.dto.AuthorDto
import com.library.elib.services.AuthorService
import com.library.elib.entities.Author
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers


@WebMvcTest(AuthorController::class)
@RunWith(SpringRunner::class) // auto spr
internal class AuthorControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @MockBean
    lateinit var authorService: AuthorService

    private val author = Author(id = 3, name = "Hello")
    private val authorList = listOf<AuthorDto>(AuthorDto(id = 3, name = "Welcome"),
        AuthorDto(id = 4, name = "Bye Bye"))


    @Test
    @Throws(Exception::class)
    fun postAuthorsUnit(){
            val jsonString = objectMapper.writeValueAsString(author)
            Mockito.`when`(authorService.addAuthor((author))).thenReturn(author)

            mockMvc.perform(
                MockMvcRequestBuilders.post("/author/create")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(jsonString)
            ).andExpect(MockMvcResultMatchers.status().isCreated)
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.`is`("Hello")))
    }

    @Test
    @Throws(Exception::class)
    fun getFilterAuthorsUnit(){
        val limit =  2
        val offset = 0
        val jsonString = objectMapper.writeValueAsString(authorList)
        Mockito.`when`(authorService.pageSearch(limit, offset)).thenReturn(authorList)

        mockMvc.perform(
            MockMvcRequestBuilders.get("/author/v2/${limit}/fetch/${offset}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString)
        ).andExpect(MockMvcResultMatchers.status().isFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", Matchers.`is`("Bye Bye")))
    }
}