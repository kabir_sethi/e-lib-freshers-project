package com.library.elib.Unit.controllersTest


import com.library.elib.controllers.BookController
import com.library.elib.dto.BookDto
import com.library.elib.entities.Author
import com.library.elib.entities.Book
import com.library.elib.services.AuthorService
import com.library.elib.services.BookService
import org.junit.Assert
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class BookControllerUnitTest {

    @InjectMocks
    private lateinit var bookController: BookController

    @Mock
    private lateinit var bookService: BookService

    @Mock
    private  lateinit var authorService: AuthorService

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    private val mockAuthor = Author(id = 1, "Paani Wala")
    private val mockBook = Book(
        id = 5,
        name = "Chao",
        page_no = 420,
        isbn_no = "ISBN 1-56389-668-1",
        categories = listOf("DRAMA", "FICTION"),
        author = mockAuthor,
        added_on = Date()
    )

    private val mockBookDto = BookDto(
        id = 5,
        name = "Choa",
        page_no = 420,
        isbn_no = "ISBN 1-56389-668-1",
        categories = listOf("DRAMA", "FICTION"),
        author_id = "1",
        added_on = Date()
    )

    private val mockFilterResult = mutableListOf( mapOf<String, Any>(
            "id" to 5,
            "name" to "Wonder Land",
            "page_no" to 420,
            "isbn_no" to "ISBN 1-56389-668-1",
            "categories" to listOf("DRAMA", "FICTION"),
            "author_id" to "3",
            "added_on" to Date()
        )
    )

    @Test
    fun createBookUnit(){
        bookService = Mockito.mock(BookService::class.java)
        Mockito.`when`(bookService.addBook(mockBookDto)).thenReturn(mockBook)

        val result = bookController.create(mockBookDto)
        Assert.assertEquals("201 CREATED", result.statusCode.toString())

    }

    @Test
    fun filterFetchUnit(){
        bookService = Mockito.mock(BookService::class.java)
        val limit = 1
        val offset = 0
        val authorId: Long = 3
        val pattern = "N"
        Mockito.`when`(bookService.pageSearchPattern( limit, offset, authorId, pattern)).thenReturn(mockFilterResult)
        bookController = Mockito.mock(BookController::class.java)
        val result = bookController.fetchFilter(limit, offset, authorId, pattern)

        Assert.assertNotNull(result)
        Assert.assertEquals(mockFilterResult[0]["isbn_no"], result[0]["isbn_no"]) // Have to override equals for this to pass or use ArgumentMatchers
        println(mockFilterResult.size)
    }

}