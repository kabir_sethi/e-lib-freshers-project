package com.library.elib

import com.library.elib.controllers.AuthorController
import com.library.elib.services.AuthorService
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest


@SpringBootTest
class ElibApplicationTests {

	@Autowired
	private lateinit var authorController: AuthorController

	@Autowired
	private lateinit var authorService: AuthorService

	val log = LoggerFactory.getLogger(javaClass)

	@Test
	@Throws(Exception::class)
	fun contextLoads() {
		log.info("Testing AuthorService availability.")
		assertThat(authorService).isNotNull
	}



}
