package com.library.elib.utils

import java.sql.Blob
import javax.sql.rowset.serial.SerialBlob

class StringBinaryConverter {

    companion object{
        fun stringToBlob(readable: String): Blob {
            return SerialBlob(readable.toByteArray())
        }

        fun blobToString(binary: Blob): String {
            return String(binary.getBytes(1, binary.length().toInt())) // Average books have less than 10000000 characters and upmost 50000000(2000 characters per page, no of pages are 25000)
        }
    }

}