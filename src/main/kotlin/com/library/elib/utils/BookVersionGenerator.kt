package com.library.elib.utils

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Service

@Service
class BookVersionGenerator(private val jdbcTemplate: JdbcTemplate){

    fun generateVersion(book_id: Long): Long {
        val statement = "SELECT COUNT(*) FROM book_pdf WHERE book_id = ?"
        val countEntry = jdbcTemplate.queryForList(statement, book_id)
        println("Version:"+countEntry[0]["COUNT(*)"])
        return countEntry[0]["COUNT(*)"] as Long
    }

}