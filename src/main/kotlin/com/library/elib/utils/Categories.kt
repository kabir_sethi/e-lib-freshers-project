package com.library.elib.utils

enum class Categories(val category: String) {

    DRAMA("Drama"),
    FICTION("Fiction"),
    ROMANCE("Romance"),
    CRIME("Crime"),
    THRILLER("Thriller"),
    HORROR("Horror");

    companion object{
        fun getList(): List<String>{
            val list = mutableListOf<String>()
            Categories.values().forEach { category ->
                list.add(category.name)
            }
            return list
        }
    }

}

