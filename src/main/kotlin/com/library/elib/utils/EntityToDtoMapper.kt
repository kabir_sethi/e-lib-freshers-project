package com.library.elib.utils

import com.library.elib.dto.AuthorDto
import com.library.elib.dto.BookDto
import com.library.elib.entities.Book

class EntityToDtoMapper {
    companion object{
        fun mapToBookDto(books: MutableList<Book>): List<BookDto> {
            val bookList: MutableList<BookDto> = mutableListOf()
            for(book in books){
                bookList.add(
                    BookDto(
                        id = book.id,
                        name = book.name,
                        author_id = book.author.id.toString(),
                        page_no = book.page_no,
                        isbn_no = book.isbn_no,
                        categories = book.categories,
                        book_pdfs = book.book_pdfs,
                        added_on = book.added_on,
                    )
                )
            }
            return bookList
        }

        fun mapToAuthorDto(authors: MutableList<Map<String, Any>>): List<AuthorDto> {
            val authorList = mutableListOf<AuthorDto>()
            for(author in authors){
                authorList.add(
                    AuthorDto(
                        id = author["id"] as Long,
                        name = author["name"] as String
                    )
                )
            }
            return authorList
        }
    }
}