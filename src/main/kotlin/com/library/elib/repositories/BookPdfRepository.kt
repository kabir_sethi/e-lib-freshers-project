package com.library.elib.repositories

import com.library.elib.entities.BookPdf
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BookPdfRepository: JpaRepository<BookPdf, Long>