package com.library.elib.controllers

import com.library.elib.dto.BookDto
import com.library.elib.exceptions.DataBaseException
import com.library.elib.exceptions.InvalidInputException
import com.library.elib.services.BookService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.Cacheable
import org.springframework.core.convert.ConversionFailedException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.reflect.UndeclaredThrowableException
import javax.validation.ConstraintViolationException

@RestController
@RequestMapping("/book")
class BookController{

    @Autowired
    private lateinit var bookService: BookService

    private val log = LoggerFactory.getLogger(javaClass)

    @PostMapping("/create")
    fun create(@RequestBody body: BookDto): ResponseEntity<Any> {
        return try{
            val book = bookService.addBook(body)
            ResponseEntity<Any>(book, HttpStatus.CREATED)
        }catch(e: Exception){
            val ex = if(e is UndeclaredThrowableException) e.undeclaredThrowable
            else e
            if(e is ConstraintViolationException) throw e
            else if(ex is InvalidInputException) throw e
            else if(ex is ConversionFailedException) throw DataBaseException("Author for the book to be added, doesn't exist.")
            else ResponseEntity<Any>(e.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @DeleteMapping("/remove/{id}")
    fun remove(@PathVariable(value = "id") book_id: Long): ResponseEntity<Any> {
        return try{
            println("Book_id $book_id")
            bookService.removeById(book_id)
            ResponseEntity<Any>("Book removed successfully.", HttpStatus.NO_CONTENT)
        } catch(e: Exception){
            println(e.message)
            println(e is org.springframework.dao.EmptyResultDataAccessException)
            if(e is org.springframework.dao.EmptyResultDataAccessException) throw DataBaseException("The book you requested to delete, doesn't exist.")
            ResponseEntity<Any>("Error: ${e.message}", HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @GetMapping("/fetch")
    fun fetch(): ResponseEntity<Any> {
        val books = bookService.fetchAll()
        return ResponseEntity<Any>(books, HttpStatus.FOUND)
    }

    @GetMapping("/fetchById")
    fun fetchById(@RequestParam id: Long): ResponseEntity<Any> = ResponseEntity<Any>(bookService.fetchById(id), HttpStatus.FOUND)


    @GetMapping("/v2/{limit}/fetch/{offset}")
    fun fetchPage(@PathVariable(value = "limit") limit: Int,
                  @PathVariable(value = "offset") offset: Int,
                  @RequestParam author_id: Long): ResponseEntity<Any> {

        return try {
            val bookList = bookService.pageSearch(limit, offset, author_id)
            ResponseEntity<Any>(bookList, HttpStatus.FOUND)
        } catch (e: Exception){
            val ex = if(e is UndeclaredThrowableException) e.undeclaredThrowable
            else e
            if(ex is DataBaseException) throw ex // for now
            else ResponseEntity<Any>("Error", HttpStatus.INTERNAL_SERVER_ERROR)
        }

    }

    @Cacheable(value = ["bookCache"], key = "#pattern")
    @GetMapping("/v3/{limit}/fetch/{offset}")
    fun fetchFilter(@PathVariable(value = "limit") limit: Int,
                    @PathVariable(value = "offset") offset: Int,
                    @RequestParam(required = false) author_id: Long?,
                    @RequestParam pattern: String): MutableList<Map<String, Any>>{
        //log.info("Hit!")
        return bookService.pageSearchPattern(limit, offset, author_id, pattern)
    }
}

