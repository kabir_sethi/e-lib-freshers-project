package com.library.elib.controllers

import com.library.elib.exceptions.InvalidInputException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/home")
class HomeController(private var jdbcTemplate: JdbcTemplate) {

    @GetMapping
    fun generateVersion(@RequestParam book_id: Long): Long {
        //throw InvalidInputException("Message")
        val statement = "SELECT COUNT(*) FROM book_pdf WHERE book_id = ?"
        val countEntry = jdbcTemplate.queryForList(statement, book_id)
        println("Version:"+countEntry[0]["COUNT(*)"])
        return countEntry[0]["COUNT(*)"] as Long + 1
    }
}