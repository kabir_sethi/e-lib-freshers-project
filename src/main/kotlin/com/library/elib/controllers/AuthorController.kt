package com.library.elib.controllers

import com.library.elib.entities.Author
import com.library.elib.repositories.AuthorRepository
import com.library.elib.services.AuthorService
import org.springframework.beans.factory.annotation.Autowired

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/author")
class AuthorController {

    @Autowired
    private lateinit var authorService: AuthorService

    @PostMapping("/create")
    fun create(@RequestBody author: Author): ResponseEntity<Any>{
        authorService.addAuthor(author)
        return ResponseEntity<Any>(author,HttpStatus.CREATED)
    }

    @GetMapping("/fetch")
    fun fetch(): Any {
        val authors = authorService.fetchAllAuthors()
        return ResponseEntity<Any>(authors,HttpStatus.FOUND)
    }

    @GetMapping("/v2/{limit}/fetch/{offset}")
    fun fetchPage(@PathVariable(value = "limit") limit: Int, @PathVariable(value = "offset") offset: Int): ResponseEntity<Any> {
        val authorList = authorService.pageSearch(limit, offset)
        return ResponseEntity<Any>(authorList,HttpStatus.FOUND)
    }
}