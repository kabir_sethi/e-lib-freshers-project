package com.library.elib.controllers

import com.library.elib.entities.BookPdf
import com.library.elib.exceptions.DataBaseException
import com.library.elib.exceptions.InvalidInputException
import com.library.elib.repositories.BookPdfRepository
import com.library.elib.services.BookPdfService
import com.library.elib.utils.BookVersionGenerator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.convert.ConversionFailedException
import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.io.ByteArrayInputStream
import java.lang.IndexOutOfBoundsException
import java.lang.reflect.UndeclaredThrowableException
import javax.validation.ConstraintViolationException

@RestController
@RequestMapping("/book_pdf")
class BookPdfController {

    @Autowired
    private lateinit var bookPdfService: BookPdfService

    @PostMapping("/upload")
    fun upload(@RequestParam("file") file: MultipartFile, @RequestParam book_id: Long): ResponseEntity<Any> {
        return try{
            bookPdfService.uploadPdf(file, book_id)
            ResponseEntity<Any>("File uploaded successfully.", HttpStatus.CREATED)
        } catch(e: Exception){
            val ex = if(e is UndeclaredThrowableException) e.undeclaredThrowable
            else e
            if(ex is InvalidInputException) throw e
            else ResponseEntity<Any>(e.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @GetMapping("/{book_id}/download/{version}")
    fun download(@PathVariable(name = "version") version: Long, @PathVariable(name = "book_id") book_id: Long): ResponseEntity<Any> {
        return try{
            val data = bookPdfService.downloadPdf(version, book_id)
            ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/pdf"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"${data.keys.elementAt(0)}\"")
                .body(data.values.elementAt(0))
        } catch(e: Exception){
            if(e is IndexOutOfBoundsException) throw InvalidInputException("Invalid version/book_id.")
            else ResponseEntity<Any>(e.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }

    }

}