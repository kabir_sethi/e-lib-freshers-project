package com.library.elib.controllers

import com.library.elib.services.TestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller("/test")
class TestController {

    @Autowired
    private lateinit var testService: TestService

    @GetMapping("/sayHi")
    fun getGreetings(): String {
        return testService.greet()
    }
}