package com.library.elib.services

import com.library.elib.dto.BookDto
import com.library.elib.entities.Author
import com.library.elib.entities.Book
import com.library.elib.exceptions.DataBaseException
import com.library.elib.repositories.AuthorRepository
import com.library.elib.repositories.BookRepository
import com.library.elib.utils.EntityToDtoMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.convert.ConversionService
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Service
import java.util.*

@Service
class BookService {

    @Autowired
    private lateinit var conversionService: ConversionService

    @Autowired
    private lateinit var bookRepository: BookRepository

    @Autowired
    private lateinit var authorRepository: AuthorRepository

    @Autowired
    private lateinit var jdbcTemplate: JdbcTemplate


    fun addBook(body: BookDto): Book{
        val author = conversionService.convert(body.author_id, Author::class.javaObjectType)
        var book = Book(
            name = body.name,
            page_no = body.page_no,
            isbn_no = body.isbn_no,
            categories = body.categories,
            author = author as Author
        )

        return bookRepository.save(book)
    }

    fun removeById(id: Long) = bookRepository.deleteById(id)

    fun fetchAll(): List<Book> = bookRepository.findAll()

    fun fetchById(id: Long): Book = bookRepository.findById(id).get()

    fun pageSearch(limit: Int, offset: Int, author_id: Long): List<BookDto> {
        val author = authorRepository.findById(author_id)
        return if(!author.isEmpty) {
            val books = author.get().books
            return if (books.isEmpty()) books as List<BookDto>
                else EntityToDtoMapper.mapToBookDto((books)).subList(offset , offset + limit )
        }else throw DataBaseException("Unable to fetch the books for this unknown author.")
    }

    fun pageSearchPattern(limit: Int, offset: Int, author_id: Long?, pattern: String): MutableList<Map<String, Any>> {
        var bookEntries: MutableList<Map<String, Any>> = if(author_id != null){
            val statement = "SELECT * FROM book WHERE (name LIKE ? OR name LIKE ? OR name LIKE ?) AND author_id = ? LIMIT ? OFFSET ?"
            jdbcTemplate.queryForList(statement, "$pattern%", "%$pattern", "%$pattern%", author_id, limit, offset)
        }else{
            val statement = "SELECT * FROM book WHERE (name LIKE ? OR name LIKE ? OR name LIKE ?) LIMIT ? OFFSET ?"
            jdbcTemplate.queryForList(statement, "$pattern%", "%$pattern", "%$pattern%", limit, offset)
        }
        return bookEntries
    }

}