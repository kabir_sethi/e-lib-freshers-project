package com.library.elib.services

import com.library.elib.dto.AuthorDto
import com.library.elib.entities.Author
import com.library.elib.repositories.AuthorRepository
import com.library.elib.utils.EntityToDtoMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException


@Service
class AuthorService {

    @Autowired
    private lateinit var authorRepository: AuthorRepository

    @Autowired
    private lateinit var jdbcTemplate: JdbcTemplate

    fun getAuthorById(id: Long): Author{
            val author = authorRepository.findById(id)
            if(author.isEmpty) throw EntityNotFoundException()
            else return author.get()
    }


    fun addAuthor(author: Author): Author = authorRepository.save(author)

    fun fetchAllAuthors(): List<Author> = authorRepository.findAll()

    fun pageSearch(limit: Int, offset: Int): List<AuthorDto> {
        val statement = "SELECT * from author LIMIT ? OFFSET ?"
        val authors = jdbcTemplate.queryForList(statement, limit, offset)
        println("Row/s: ${authors.size}")
        println(authors[0]["name"])
        return EntityToDtoMapper.mapToAuthorDto(authors)
    }

}