package com.library.elib.services

import org.springframework.stereotype.Service

@Service
class TestService {
    fun greet(): String {
        return "Hello"
    }
}