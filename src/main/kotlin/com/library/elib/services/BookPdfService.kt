package com.library.elib.services

import com.library.elib.entities.BookPdf
import com.library.elib.exceptions.InvalidInputException
import com.library.elib.repositories.BookPdfRepository
import com.library.elib.utils.BookVersionGenerator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Service
import org.springframework.util.StringUtils
import org.springframework.web.multipart.MultipartFile
import java.io.ByteArrayInputStream

@Service
class BookPdfService{

    @Autowired
    private lateinit var bookVersionGenerator: BookVersionGenerator

    @Autowired
    private lateinit var bookPdfRepository: BookPdfRepository

    @Autowired
    private lateinit var jdbcTemplate: JdbcTemplate

    fun uploadPdf(file: MultipartFile, book_id: Long) {
        if(!file.isEmpty) {
            val fileName = StringUtils.cleanPath(file?.originalFilename ?: "No_Name")
            val data = file.bytes
            val version = bookVersionGenerator.generateVersion(book_id) + 1
            val book = BookPdf(version = version, name = fileName, file = data, book_id = book_id)
            bookPdfRepository.save(book)
        }else throw InvalidInputException("File not found.")
    }

    fun downloadPdf(version: Long, book_id: Long): Map<String, InputStreamResource>{
        val statement = "SELECT file, name FROM book_pdf WHERE book_id = ? AND version = ?"
        val book = jdbcTemplate.queryForList(statement,book_id, version)
        val fileBytes = book[0]["file"] as ByteArray
        val fileName = book[0]["name"] as String
        val inputStreamResource = InputStreamResource(ByteArrayInputStream(fileBytes))
        return mapOf(fileName to inputStreamResource)
    }

}