package com.library.elib.exceptions

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import java.lang.reflect.UndeclaredThrowableException

@EnableWebMvc
@ControllerAdvice
class InvalidInputAndDataBaseExceptionHandler {
    @ExceptionHandler(value = [ InvalidInputException::class, DataBaseException::class])
    fun handleIIE(e: Exception, wr: WebRequest): ResponseEntity<String> {
        var iadbeh: Throwable = if(e is UndeclaredThrowableException) (e as UndeclaredThrowableException).undeclaredThrowable
        else e
        return ResponseEntity<String>(iadbeh.message, HttpStatus.BAD_REQUEST)
    }
}