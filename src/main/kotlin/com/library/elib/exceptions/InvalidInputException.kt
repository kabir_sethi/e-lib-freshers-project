package com.library.elib.exceptions

class InvalidInputException(message: String): Exception(message)