package com.library.elib.exceptions

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.lang.reflect.UndeclaredThrowableException
import javax.persistence.EntityNotFoundException
import javax.validation.ConstraintViolationException

@EnableWebMvc
@ControllerAdvice
class ConstraintViolationsExceptionHandler: ResponseEntityExceptionHandler() {
    @ExceptionHandler(value = [javax.validation.ConstraintViolationException::class,
        javax.persistence.EntityNotFoundException::class
    ])
    fun handeleCVE(e: Exception, wr: WebRequest): ResponseEntity<String> {
        println("Here! Here! Here!")
        var cve = e as ConstraintViolationException
        return ResponseEntity<String>(cve.constraintViolations.elementAt(0).message, HttpStatus.BAD_REQUEST)
    }

}