package com.library.elib.exceptions

class DataBaseException(message: String): Exception(message)