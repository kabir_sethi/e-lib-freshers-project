package com.library.elib.converters

import com.library.elib.exceptions.InvalidInputException
import com.library.elib.utils.Categories
import net.bytebuddy.implementation.bytecode.Throw
import javax.persistence.AttributeConverter
import javax.persistence.Converter
import javax.validation.ConstraintViolationException
import kotlin.jvm.Throws

@Converter(autoApply = true)
class StringListToEnumList: AttributeConverter<List<String>,String>/*Converter<List<String>, String> */ {

    @Throws(InvalidInputException::class)
    override fun convertToDatabaseColumn(attribute: List<String>): String {
            val categoryList: List<String> =  Categories.getList()
            if(categoryList.containsAll(attribute)) return attribute.stream().toArray().joinToString(",")
            else throw InvalidInputException("Invalid Categories")
    }

    override fun convertToEntityAttribute(dbData: String): List<String> {
        val list:  MutableList<String> = mutableListOf<String>()
        dbData.split(',').toTypedArray().forEach { category ->
            list.add(category)
        }
        return list
    }
}