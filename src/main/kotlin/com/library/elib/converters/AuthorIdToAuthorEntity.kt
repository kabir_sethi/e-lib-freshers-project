package com.library.elib.converters

import com.library.elib.entities.Author
import com.library.elib.services.AuthorService
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class AuthorIdToAuthorEntity(private val authorService: AuthorService): Converter<String, Author> {

    override fun convert(source: String): Author {
        try{
            return authorService.getAuthorById(source.toLong())
        }catch(e: Exception){
            e.printStackTrace()
            throw e
        }
    }

}