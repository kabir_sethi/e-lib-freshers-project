package com.library.elib.config

import com.library.elib.converters.AuthorIdToAuthorEntity
import com.library.elib.services.AuthorService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.format.FormatterRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@Configuration
class ConverterConfig(): WebMvcConfigurer {

    @Autowired(required = true)
    private lateinit var authorService: AuthorService

    override fun addFormatters(registry: FormatterRegistry) {
        registry.addConverter(AuthorIdToAuthorEntity(authorService))
    }
}