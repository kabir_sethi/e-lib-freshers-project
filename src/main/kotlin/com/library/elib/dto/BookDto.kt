package com.library.elib.dto

import com.library.elib.entities.BookPdf
import java.util.*

data class BookDto (

    val id: Long?,

    val name: String = "",

    val page_no: Int = 0,

    val isbn_no: String = "",

    val author_id: String,

    val added_on: Date?,

    val book_pdfs: MutableList<BookPdf>? = mutableListOf(),

    val categories: List<String> = listOf(),

    )