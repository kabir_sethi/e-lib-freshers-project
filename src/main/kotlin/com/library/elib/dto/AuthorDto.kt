package com.library.elib.dto

class AuthorDto(val id: Long = 0, val name: String = "")