package com.library.elib.dto

import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable

@Serializable
class Test (
    val name: String =""
    )