package  com.library.elib.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonManagedReference
import javax.persistence.Entity
import javax.persistence.*
import javax.persistence.Id

@Entity(name = "Author")
@Table(name = "author")
data class Author(

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    val name: String = "",

    @JsonIgnoreProperties("books")
    @OneToMany(mappedBy = "author", cascade = [CascadeType.ALL], orphanRemoval = true)
    @JsonManagedReference
    var books: MutableList<Book> = mutableListOf()
)