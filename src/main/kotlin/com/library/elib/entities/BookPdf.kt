package com.library.elib.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import kotlinx.serialization.Contextual

import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity(name = "BookPdf")
@Table(name = "book_pdf")
data class BookPdf(

    @Id
    @GeneratedValue
    val id: Long = 0,

    @Column
    val version: Long = 0,

    @Column
    val name: String = " ",

    @Column
    @get: NotNull
    val book_id: Long = 0,

    @Column
    @Lob
    @JsonIgnore
    val file: ByteArray= byteArrayOf()

) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BookPdf

        if (id != other.id) return false
        if (version != other.version) return false
        if (book_id != other.book_id) return false
        if (!file.contentEquals(other.file)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + version.hashCode()
        result = 31 * result + book_id.hashCode()
        result = 31 * result + file.contentHashCode()
        return result
    }
}

