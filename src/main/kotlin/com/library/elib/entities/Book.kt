package  com.library.elib.entities

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonManagedReference
import com.library.elib.converters.StringListToEnumList
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import org.springframework.lang.Nullable
import java.sql.Blob
import java.util.Date

import javax.persistence.*
import javax.validation.constraints.*

@Entity(name = "Book")
@Table(name = "book")
@Serializable
data class Book (

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    @get: NotBlank
    @Column(unique = true)
    @get: Size(min = 2, max = 30, message = "Title length must be b/w 2 and 30")
    val name: String = "",

    @get: Min(5)
    @get: Max(2147483647) // Int.MAX_VALUE
    val page_no: Int = 0,

    @Column(unique = true)
    @field: Pattern(regexp = "ISBN\\x20(?=.{13}\$)\\d{1,5}([- ])\\d{1,7}\\1\\d{1,6}\\1(\\d|X)\$", message = "Invalid ISBN no.")
    val isbn_no: String = "",

    @NotNull
    @Temporal(TemporalType.DATE)
    val added_on: Date = Date(),

// ---------------------------------------------------


    @OneToMany( cascade = [CascadeType.ALL], orphanRemoval = true)
    @JoinColumn(name = "book_id")

    val book_pdfs: MutableList<BookPdf> = mutableListOf(),

// ---------------------------------------------------


//    @Column(unique = true) // Content cannot be copied
//    @field: Nullable()
//    @Lob
//    val pdf_bin: Blob?,

    @get: NotNull
    @Convert(converter = StringListToEnumList::class)
    val categories: List<String> = listOf(), // Join all enum String separated by ','

    @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    @JsonBackReference
    var author: Author = Author()


)
