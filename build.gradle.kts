//import jdk.internal.vm.vector.VectorSupport.test
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	application
	id("org.springframework.boot") version "2.5.3"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.5.21"
	kotlin("plugin.spring") version "1.5.21"
	kotlin("plugin.jpa") version "1.5.21"
}

group = "com.library"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {

	implementation("org.springframework.boot:spring-boot-starter-data-jdbc")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("mysql:mysql-connector-java:8.0.26")
	implementation("org.springframework.boot:spring-boot-starter-validation")
	implementation("javax.validation:validation-api")
	implementation("com.fasterxml.jackson.core:jackson-databind:2.12.3")
	implementation("org.springframework.boot:spring-boot-starter-actuator:2.5.2")
	implementation("io.springfox:springfox-swagger2:2.9.2")
	implementation("io.springfox:springfox-swagger-ui:2.9.2")
    implementation("junit:junit:4.13.1")
	implementation("junit:junit:4.13.1")
	implementation("log4j:log4j:1.2.17")
	implementation("org.springframework.boot:spring-boot-starter-cache:2.5.3")
	implementation("org.springframework.boot:spring-boot-starter-data-redis:2.5.3")
	implementation("org.jetbrains.kotlinx:kotlinx-serialization-json-jvm:1.2.2")
	testImplementation("io.mockk:mockk:1.9.3")

	testImplementation("org.mock-server:mockserver-netty:5.11.1")
	testImplementation("org.mock-server:mockserver-client-java:5.11.1")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

